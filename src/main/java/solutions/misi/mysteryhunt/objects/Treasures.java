package solutions.misi.mysteryhunt.objects;

import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.mysteryhunt.MysteryHunt;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Treasures {

	public HashMap<Location, String> treasures = new HashMap<>();
	public HashMap<Location, String> despawning = new HashMap<>();
	public ArrayList<Parachute> parachutes;
	private ArrayList<Integer> usedRewards;

	public void loadRewards(Player p, Inventory inv, String treasure) {
		usedRewards = new ArrayList<>();
		parachutes = new ArrayList<>();

		String rewardsAmountString = MysteryHunt.getInstance().getConfig().getString("treasures." + treasure + ".rewards-amount");
		int rewardsAmount;
		File file_points = new File(MysteryHunt.getInstance().getDataFolder(), "points.yml");
		if (!file_points.exists()) {
			try {
				file_points.createNewFile();
			} catch (IOException var26) {
				var26.printStackTrace();
			}
		}

		FileConfiguration points = MysteryHunt.getInstance().fileManager.getFileConfiguratioon(file_points);
		int pointsReward = MysteryHunt.getInstance().getConfig().getInt("treasures." + treasure + ".open-points") + points.getInt(p.getUniqueId().toString());
		points.set(p.getUniqueId().toString(), pointsReward);

		try {
			points.save(file_points);
		} catch (IOException var25) {
			var25.printStackTrace();
		}

		if (rewardsAmountString.contains("-")) {
			String[] rewardsAmountStringArray = rewardsAmountString.split("-");
            int rewardsAmountMin = Integer.parseInt(rewardsAmountStringArray[0]);
            int rewardsAmountMax = Integer.parseInt(rewardsAmountStringArray[1]);

            Random random = new Random();
            rewardsAmount = random.nextInt(rewardsAmountMax - rewardsAmountMin) + rewardsAmountMin;
        } else {
            rewardsAmount = Integer.parseInt(rewardsAmountString);
		}
		
		int availableRewardsCount = getAvailableRewardsCount(treasure);
		
		for(int i = 0; i < rewardsAmount; i++) {
            int selectedReward = -1;

            while (selectedReward == -1) {
                selectedReward = getRandomSelection(availableRewardsCount, treasure);
            }

            String type = MysteryHunt.getInstance().getConfig().getString("treasures." + treasure + ".rewards." + selectedReward + ".type");
            String executeAmountString = MysteryHunt.getInstance().getConfig().getString("treasures." + treasure + ".rewards." + selectedReward + ".amount");
            int executeAmount;

            if (executeAmountString.contains("-")) {
                String[] executeAmountStringArray = executeAmountString.split("-");
                int executeAmountMin = Integer.parseInt(executeAmountStringArray[0]);
                int executeAmountMax = Integer.parseInt(executeAmountStringArray[1]);
                Random rand = new Random();
                executeAmount = rand.nextInt(executeAmountMax - executeAmountMin) + executeAmountMin;
            } else {
                executeAmount = Integer.parseInt(executeAmountString);
            }
			
			if(type.equalsIgnoreCase("command")) {
                String command = ChatColor.translateAlternateColorCodes('&', MysteryHunt.getInstance().getConfig().getString("treasures." + treasure + ".rewards." + selectedReward + ".value")).replaceAll("%player%", p.getName()).replaceAll("%p_x%", p.getLocation().getX() + "").replaceAll("%p_y%", p.getLocation().getY() + "").replaceAll("%p_z%", p.getLocation().getZ() + "");

                if (MysteryHunt.getInstance().dependencyManager.getPlaceholderApi()) {
                    command = PlaceholderAPI.setPlaceholders(p, command);
                }

                for (int i1 = 0; i1 < executeAmount; i1++) {
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
                }
            } else if(type.equalsIgnoreCase("item")) {
				Material material = Material.getMaterial(MysteryHunt.getInstance().getConfig().getString("treasures." + treasure + ".rewards." + selectedReward + ".value"));
				String name = ChatColor.translateAlternateColorCodes('&', MysteryHunt.getInstance().getConfig().getString("treasures." + treasure + ".rewards." + selectedReward + ".name"));
				List<String> lore = new ArrayList<String>();

				for(String loreEntry : MysteryHunt.getInstance().getConfig().getStringList("treasures." + treasure + ".rewards." + selectedReward + ".lore")) {
					lore.add(ChatColor.translateAlternateColorCodes('&', loreEntry));
				}
				
				ItemStack item = new ItemStack(material, executeAmount);
				ItemMeta itemMeta = item.getItemMeta();
				itemMeta.setDisplayName(name);
				itemMeta.setLore(lore);
				
				for(String enchantsEntry : MysteryHunt.getInstance().getConfig().getStringList("treasures." + treasure + ".rewards." + selectedReward + ".enchantments")) {
					String[] enchantmentsArray = enchantsEntry.split(" ");
					String enchantment = enchantmentsArray[0];
					String levelString = enchantmentsArray[1];

					int level;

					if (levelString.contains("-")) {
						String[] levelStringArray = levelString.split("-");
						int levelMin = Integer.parseInt(levelStringArray[0]);
						int levelMax = Integer.parseInt(levelStringArray[1]);
						Random rand = new Random();
						level = rand.nextInt(levelMax - levelMin) + levelMin;
					} else {
						level = Integer.parseInt(levelString);
					}

					item.addUnsafeEnchantment(Enchantment.getByName(enchantment), level);
				}
				
				item.setItemMeta(itemMeta);
				
				int randomSlot = 0;
				while(inv.getItem(randomSlot) != null) {
					Random ran = new Random();
					randomSlot = ran.nextInt(inv.getSize());
				}
				
				inv.setItem(randomSlot, item);
			}
		}
	}
	
	public int getAvailableRewardsCount(String treasure) {
		for(int i = 0; i < 999; i++) {
			if(MysteryHunt.getInstance().getConfig().get("treasures." + treasure + ".rewards." + i) == null) {
				return i;
			}
		}
		return 0;
	}
	
	public int getRandomSelection(int rewardsAmount, String treasure) {
		Random random = new Random();
		int selectedReward = random.nextInt(rewardsAmount);
		
		if(!MysteryHunt.getInstance().getConfig().getBoolean("treasures." + treasure + ".same-rewards")) {
			if(!usedRewards.contains(selectedReward)) {
				if(MysteryHunt.getInstance().getConfig().get("treasures." + treasure + ".rewards." + selectedReward) != null) {
					usedRewards.add(selectedReward);
					return selectedReward;
				} 
			}
		} else {
			if(MysteryHunt.getInstance().getConfig().get("treasures." + treasure + ".rewards." + selectedReward) != null) {
				return selectedReward;
			} 
		}
		return -1;
	}
	
	public Location findNearestTreasure(Location playerLocation) {
		Location nearestTreasureLocation = null;
		
		for(Location loc : MysteryHunt.getInstance().treasures.treasures.keySet()) {
			if(loc.getWorld().getName() == playerLocation.getWorld().getName()) {
				if(nearestTreasureLocation != null) {
					if(playerLocation.distance(loc) < playerLocation.distance(nearestTreasureLocation)) {
						nearestTreasureLocation = loc;
					}
				} else {
					nearestTreasureLocation = loc;
				}
			}
		}
		
		return nearestTreasureLocation;
	}
	
	public void destroyAllTreasures() {
		for (Location loc : MysteryHunt.getInstance().treasures.treasures.keySet()) {
			loc.getWorld().loadChunk(loc.getWorld().getChunkAt(loc.getBlock()));
			loc.getBlock().setType(Material.AIR);
			loc.clone().subtract(0, 1, 0).getBlock().setType(Material.AIR);
		}

		for (Location loc : MysteryHunt.getInstance().treasures.despawning.keySet()) {
			loc.getBlock().setType(Material.AIR);
			loc.clone().subtract(0, 1, 0).getBlock().setType(Material.AIR);
		}

		for (Map.Entry<Location, Material> entry : MysteryHunt.getInstance().locationHandler.getBlocksUnderneath().entrySet()) {
			entry.getKey().getBlock().setType(entry.getValue());
		}

		for(Parachute parachute : MysteryHunt.getInstance().treasures.parachutes) {
			parachute.destroy();
		}

		MysteryHunt.getInstance().treasures.treasures.clear();
		MysteryHunt.getInstance().treasures.despawning.clear();
		MysteryHunt.getInstance().treasures.parachutes.clear();
		MysteryHunt.getInstance().locationHandler.getBlocksUnderneath().clear();
	}

	public void destroyAllTreasures(String treasure) {
		for (Location loc : MysteryHunt.getInstance().treasures.treasures.keySet()) {
			if(!MysteryHunt.getInstance().treasures.treasures.get(loc).equals(treasure)) continue;
			loc.getBlock().setType(Material.AIR);
			loc.clone().subtract(0, 1, 0).getBlock().setType(Material.AIR);
		}

		for (Location loc : MysteryHunt.getInstance().treasures.despawning.keySet()) {
			if(!MysteryHunt.getInstance().treasures.treasures.get(loc).equals(treasure)) continue;
			loc.getBlock().setType(Material.AIR);
			loc.clone().subtract(0, 1, 0).getBlock().setType(Material.AIR);
		}

		for (Map.Entry<Location, Material> entry : MysteryHunt.getInstance().locationHandler.getBlocksUnderneath().entrySet()) {
			if(!MysteryHunt.getInstance().treasures.treasures.get(entry.getKey()).equals(treasure)) continue;
			entry.getKey().getBlock().setType(entry.getValue());
		}
	}
}
