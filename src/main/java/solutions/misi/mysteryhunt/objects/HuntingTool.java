package solutions.misi.mysteryhunt.objects;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.mysteryhunt.MysteryHunt;

import java.util.ArrayList;
import java.util.List;

public class HuntingTool {

    public static ItemStack getHuntingTool(String type) {
        Material material = Material.getMaterial(MysteryHunt.getInstance().getConfig().getString("huntingtools." + type + ".material"));
        String name = ChatColor.translateAlternateColorCodes('&', MysteryHunt.getInstance().getConfig().getString("huntingtools." + type + ".name"));
        List<String> lore = new ArrayList();

        for(int i = 0; i < MysteryHunt.getInstance().getConfig().getStringList("huntingtools." + type + ".lore").size(); ++i) {
            lore.add(ChatColor.translateAlternateColorCodes('&', MysteryHunt.getInstance().getConfig().getStringList("huntingtools." + type + ".lore").get(i)));
        }

        ItemStack item = new ItemStack(material);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(name);
        itemMeta.setLore(lore);
        if (MysteryHunt.getInstance().getConfig().getBoolean("huntingtools." + type + ".glow")) {
            itemMeta.addEnchant(Enchantment.LUCK, 1, true);
        }

        item.setItemMeta(itemMeta);
        return item;
    }
}
