package solutions.misi.mysteryhunt.events;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.scheduler.BukkitRunnable;
import solutions.misi.mysteryhunt.MysteryHunt;
import solutions.misi.mysteryhunt.util.CooldownManager;

import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class PlayerInteractListener implements Listener {

	private final CooldownManager cooldownManager = new CooldownManager();

	@EventHandler
	public void onInteract(final PlayerInteractEvent e) {
		Player p = e.getPlayer();

		if (e.getHand() == EquipmentSlot.HAND) {
			if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if(e.getClickedBlock().getType() == Material.CHEST) {
					Block chest = e.getClickedBlock();
					final Location loc = chest.getLocation();
					final String treasure = MysteryHunt.getInstance().treasures.treasures.get(loc);
					String openPermission = MysteryHunt.getInstance().getConfig().getString("treasures." + treasure + ".open-permission");

					if (MysteryHunt.getInstance().treasures.treasures.containsKey(new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()))) {
						if (p.isOp() || p.hasPermission(openPermission)) {
							//> open treasure
							new BukkitRunnable() {
								@Override
								public void run() {
									MysteryHunt.getInstance().treasures.loadRewards(e.getPlayer(), e.getPlayer().getOpenInventory().getTopInventory(), treasure);
									MysteryHunt.getInstance().schedulerManager.despawnOpenTreasure(loc);
                                    MysteryHunt.getInstance().treasures.treasures.remove(loc);

                                    MysteryHunt.getInstance().playersTable.setTreasuresOpened(e.getPlayer(), treasure, MysteryHunt.getInstance().playersTable.getTreasuresOpened(e.getPlayer(), treasure)+1);
                                    MysteryHunt.getInstance().serverTable.setTreasuresOpened(treasure, MysteryHunt.getInstance().serverTable.getTreasuresOpened(treasure)+1);
                                }
                            }.runTaskLater(MysteryHunt.getInstance(), 5);
                        } else {
                            e.setCancelled(true);
                            p.sendMessage(MysteryHunt.getInstance().messages.prefix + "§cYou are not allowed to open this treasure!");
                        }
					}
				}
			}

			try {
				Iterator var12 = MysteryHunt.getInstance().getConfig().getConfigurationSection("huntingtools").getKeys(false).iterator();

				while(var12.hasNext()) {
					String key = (String)var12.next();
					if (e.getPlayer().getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes('&', MysteryHunt.getInstance().getConfig().getString("huntingtools." + key + ".name")))) {
						if(!e.getPlayer().isOp() && !e.getPlayer().hasPermission(MysteryHunt.getInstance().getConfig().getString("huntingtools." + key + ".permission"))) {
							e.getPlayer().sendMessage(MysteryHunt.getInstance().messages.prefix + " §cYou don't have enough permissions!");
							return;
						}

						if(MysteryHunt.getInstance().getConfig().getLong("huntingtools." + key + ".cooldown") != 0) {
							long timeLeft = System.currentTimeMillis() - this.cooldownManager.getCooldown(e.getPlayer().getUniqueId());
							if (TimeUnit.MILLISECONDS.toSeconds(timeLeft) < MysteryHunt.getInstance().getConfig().getLong("huntingtools." + key + ".cooldown")) {
								Long cooldownTime = MysteryHunt.getInstance().getConfig().getLong("huntingtools." + key + ".cooldown") - TimeUnit.MILLISECONDS.toSeconds(timeLeft);
								e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', MysteryHunt.getInstance().getConfig().getString("messages.cooldown").replace("%cooldown%", cooldownTime.toString())));
								return;
							}
						}

						Location loc = MysteryHunt.getInstance().treasures.findNearestTreasure(p.getLocation());
						if (loc != null) {
							int distance = (int) Math.round(p.getLocation().distance(loc));
							String msg = MysteryHunt.getInstance().getConfig().getString("messages.how-many-blocks-away").replace("%treasure%", MysteryHunt.getInstance().treasures.treasures.get(loc));
							msg = msg.replace("%distance%", Integer.valueOf(distance).toString());
							msg = ChatColor.translateAlternateColorCodes('&', msg);
							p.sendMessage(MysteryHunt.getInstance().messages.prefix + msg);
						} else {
							p.sendMessage(MysteryHunt.getInstance().messages.prefix + "§cThere is no treasure nearby!");
						}

						if(MysteryHunt.getInstance().getConfig().getLong("huntingtools." + key + ".cooldown") != 0) {
							this.cooldownManager.setCooldown(p.getUniqueId(), System.currentTimeMillis());
						}
					}
				}
			} catch (NullPointerException var10) {
			}
		}
	}
}
