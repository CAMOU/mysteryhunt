package solutions.misi.mysteryhunt.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import solutions.misi.mysteryhunt.MysteryHunt;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        MysteryHunt.getInstance().playersTable.registerPlayer(event.getPlayer());
    }
}
