package solutions.misi.mysteryhunt.expansion;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.entity.Player;
import solutions.misi.mysteryhunt.MysteryHunt;

public class TreasureExpansion extends PlaceholderExpansion {

    private final MysteryHunt plugin;
    private final String treasure;

    public TreasureExpansion(MysteryHunt plugin, String treasure) {
        this.plugin = plugin;
        this.treasure = treasure;
    }

    public boolean persist() {
        return true;
    }

    public boolean canRegister() {
        return true;
    }

    public String getAuthor() {
        return this.plugin.getDescription().getAuthors().toString();
    }

    public String getIdentifier() {
        return "mysteryhunt";
    }

    public String getVersion() {
        return this.plugin.getDescription().getVersion();
    }

    public String onPlaceholderRequest(Player player, String identifier) {
        if (player == null) {
            return "";
        } else if (identifier.equals(treasure + "_total_opened")) {
            return String.valueOf(MysteryHunt.getInstance().playersTable.getTreasuresOpened(player, treasure));
        } else if (identifier.equals("total_tiers_opened")) {
            int openedTreasures = 0;
            for(String treasure : MysteryHunt.getInstance().getConfig().getConfigurationSection("treasures").getKeys(false)) {
                openedTreasures += MysteryHunt.getInstance().playersTable.getTreasuresOpened(player, treasure);
            }

            return String.valueOf(openedTreasures);
        } else {
            return null;
        }
    }

}
