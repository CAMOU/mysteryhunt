package solutions.misi.mysteryhunt.expansion;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import solutions.misi.mysteryhunt.MysteryHunt;

public class ServerTreasureExpansion extends PlaceholderExpansion {

    private final MysteryHunt plugin;
    private final String treasure;

    public ServerTreasureExpansion(MysteryHunt plugin, String treasure) {
        this.plugin = plugin;
        this.treasure = treasure;
    }

    public boolean persist() {
        return true;
    }

    public boolean canRegister() {
        return true;
    }

    public String getAuthor() {
        return this.plugin.getDescription().getAuthors().toString();
    }

    public String getIdentifier() {
        return "mysteryhunt";
    }

    public String getVersion() {
        return this.plugin.getDescription().getVersion();
    }

    public String onPlaceholderRequest(String identifier) {
        if (identifier.equals(treasure + "_total_opened_serverwide")) {
            return String.valueOf(MysteryHunt.getInstance().serverTable.getTreasuresOpened(treasure));
        } else if (identifier.equals("total_tiers_opened_serverwide")) {
            int openedTreasures = 0;
            for(String treasure : MysteryHunt.getInstance().getConfig().getConfigurationSection("treasures").getKeys(false)) {
                openedTreasures += MysteryHunt.getInstance().serverTable.getTreasuresOpened(treasure);
            }

            return String.valueOf(openedTreasures);
        } else {
            return null;
        }
    }

}
