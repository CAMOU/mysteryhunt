package solutions.misi.mysteryhunt.expansion;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import solutions.misi.mysteryhunt.MysteryHunt;

import java.io.File;

public class PointsExpansion extends PlaceholderExpansion {

    private final MysteryHunt plugin;

    public PointsExpansion(MysteryHunt plugin) {
        this.plugin = plugin;
    }

    public boolean persist() {
        return true;
    }

    public boolean canRegister() {
        return true;
    }

    public String getAuthor() {
        return this.plugin.getDescription().getAuthors().toString();
    }

    public String getIdentifier() {
        return "mysteryhunt";
    }

    public String getVersion() {
        return this.plugin.getDescription().getVersion();
    }

    public String onPlaceholderRequest(Player player, String identifier) {
        if (player == null) {
            return "";
        } else if (identifier.equals("points")) {
            File file_points = new File(this.plugin.getDataFolder(), "points.yml");
            FileConfiguration points = this.plugin.fileManager.getFileConfiguratioon(file_points);
            Integer returnInt = points.getInt(player.getUniqueId().toString());
            return returnInt.toString();
        } else {
            return null;
        }
    }

}
