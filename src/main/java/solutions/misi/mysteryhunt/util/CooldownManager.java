package solutions.misi.mysteryhunt.util;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CooldownManager {

    private final Map<UUID, Long> cooldowns = new HashMap();

    public void setCooldown(UUID player, long time) {
        if (time < 1L) {
            this.cooldowns.remove(player);
        } else {
            this.cooldowns.put(player, time);
        }

    }

    public long getCooldown(UUID player) {
        return this.cooldowns.getOrDefault(player, 0L);
    }

}
