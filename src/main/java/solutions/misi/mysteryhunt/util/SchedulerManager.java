package solutions.misi.mysteryhunt.util;

import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import solutions.misi.mysteryhunt.MysteryHunt;
import solutions.misi.mysteryhunt.objects.Parachute;

import java.util.List;

public class SchedulerManager {

    public void loadTreasures() {
        ConfigurationSection treasuresSection = MysteryHunt.getInstance().getConfig().getConfigurationSection("treasures");
        for (String treasure : treasuresSection.getKeys(false)) {
            startTreasureSpawning(treasure);
        }
    }

    public void startTreasureSpawning(final String treasure) {
        final int minPlayers = MysteryHunt.getInstance().getConfig().getInt("treasures." + treasure + ".min-players");

        if (!MysteryHunt.getInstance().getConfig().getString("treasures." + treasure + ".spawn-delay").equalsIgnoreCase("manual")) {
            int spawnDelay = Integer.parseInt(MysteryHunt.getInstance().getConfig().getString("treasures." + treasure + ".spawn-delay"));
            final List<String> worldsList = (List<String>) MysteryHunt.getInstance().getConfig().get("treasures." + treasure + ".spawn-worlds");
            final boolean fallFromSky = MysteryHunt.getInstance().getConfig().getBoolean("treasures." + treasure + ".fall-from-sky");

            new BukkitRunnable() {
                @Override
                public void run() {
                    for (int i = 0; i < worldsList.size(); i++) {
                        int playersInWorld = 0;
                        for (Player all : Bukkit.getOnlinePlayers()) {
                            if (all.getWorld().getName().equals(worldsList.get(i))) {
                                playersInWorld = playersInWorld + 1;
                            }
                        }

                        if (playersInWorld >= minPlayers) {
                            String broadcast = ChatColor.translateAlternateColorCodes('&', MysteryHunt.getInstance().getConfig().getString("treasures." + treasure + ".broadcast")).replaceAll("%world%", worldsList.get(i));
                            int maxChests = MysteryHunt.getInstance().fileManager.getFileConfiguratioon(MysteryHunt.getInstance().fileManager.getWorldsFile()).getInt(worldsList.get(i) + ".max-chests-overall");

                            if (MysteryHunt.getInstance().dependencyManager.getPlaceholderApi()) {
                                broadcast = PlaceholderAPI.setPlaceholders(null, broadcast);
                            }

                            int currentChests = 0;
                            int currentSpecificChests = 0;
                            for (Location loc : MysteryHunt.getInstance().treasures.treasures.keySet()) {
                                if (loc.getWorld().getName().equals(worldsList.get(i))) {
                                    currentChests++;
                                    if(MysteryHunt.getInstance().treasures.treasures.get(loc).equals(treasure)) currentSpecificChests++;
                                }
                            }

                            if(currentSpecificChests >= MysteryHunt.getInstance().fileManager.getFileConfiguratioon(MysteryHunt.getInstance().fileManager.getWorldsFile()).getInt(worldsList.get(i) + ".max-chests." + treasure)) {
                                return;
                            }

                            if (currentChests < maxChests) {
                                Location loc = MysteryHunt.getInstance().locationHandler.findTreasureLocation(worldsList.get(i), treasure);

                                if (fallFromSky) {
                                    new Parachute(worldsList.get(i), loc.getX(), (loc.getY() + 50), loc.getZ(), treasure);
                                } else {
                                    loc.add(0, 1, 0);
                                    Material blockUnderneath = Material.getMaterial(MysteryHunt.getInstance().getConfig().getString("treasures." + treasure + ".block-underneath"));

                                    Bukkit.getWorld(worldsList.get(i)).getBlockAt(loc).setType(Material.CHEST);
                                    MysteryHunt.getInstance().locationHandler.getBlocksUnderneath().put(loc.clone().subtract(0, 1, 0), loc.clone().subtract(0, 1, 0).getBlock().getType());
                                    Bukkit.getWorld(worldsList.get(i)).getBlockAt(loc.clone().subtract(0, 1, 0)).setType(blockUnderneath);

                                    MysteryHunt.getInstance().treasures.treasures.put(new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()), treasure);
                                }

                                MysteryHunt.getInstance().notDestroyable.put(loc, treasure);
                                despawnNaturalTreasure(loc, treasure);

                                for(int j = 0; j < 3; ++j) {
                                    final Integer time = 3 - j;

                                    Bukkit.getScheduler().scheduleSyncDelayedTask(MysteryHunt.getInstance(), () -> {
                                        for(Player player : Bukkit.getOnlinePlayers()) {
                                            if(player.getWorld() != loc.getWorld()) continue;
                                            player.sendMessage(MysteryHunt.getInstance().messages.prefix + ChatColor.translateAlternateColorCodes('&', MysteryHunt.getInstance().getConfig().getString("treasures." + treasure + ".countdown").replaceAll("%time%", time.toString())));
                                        }
                                    }, 20*j);
                                }

                                String finalBroadcast = broadcast;
                                Bukkit.getScheduler().scheduleSyncDelayedTask(MysteryHunt.getInstance(), () -> {
                                    for(Player player : Bukkit.getOnlinePlayers()) {
                                        if(player.getWorld() != loc.getWorld()) continue;
                                        player.sendMessage(MysteryHunt.getInstance().messages.prefix + finalBroadcast);
                                    }
                                }, 80L);
                            }
                        }
                    }
                }
            }.runTaskTimer(MysteryHunt.getInstance(), 20 * spawnDelay, 20 * spawnDelay);
        }
    }

    public void despawnOpenTreasure(final Location loc) {
        int despawnDelay = MysteryHunt.getInstance().getConfig().getInt("despawn-delay-for-opened-treasures");
        MysteryHunt.getInstance().treasures.despawning.put(loc, MysteryHunt.getInstance().treasures.treasures.get(loc));

        new BukkitRunnable() {
            @Override
            public void run() {
                loc.getBlock().setType(Material.AIR);
                loc.clone().subtract(0, 1, 0).getBlock().setType(Material.AIR);
                loc.getWorld().spawnParticle(Particle.CRIT_MAGIC, loc, 5);
                MysteryHunt.getInstance().notDestroyable.remove(loc);
            }
        }.runTaskLater(MysteryHunt.getInstance(), 20 * despawnDelay);
    }

    public void despawnNaturalTreasure(final Location loc, String treasure) {
        int despawnDelay = MysteryHunt.getInstance().getConfig().getInt("treasures." + treasure + ".despawn-delay");

        new BukkitRunnable() {
            @Override
            public void run() {
                if (MysteryHunt.getInstance().treasures.treasures.containsKey(loc)) {
                    loc.getBlock().setType(Material.AIR);
                    loc.getWorld().spawnParticle(Particle.CRIT_MAGIC, loc, 5);
                    MysteryHunt.getInstance().treasures.treasures.remove(loc);
                    MysteryHunt.getInstance().notDestroyable.remove(loc);
                }
            }
        }.runTaskLater(MysteryHunt.getInstance(), 20 * despawnDelay);
	}
}
