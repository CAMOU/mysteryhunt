package solutions.misi.mysteryhunt;

import com.zaxxer.hikari.HikariDataSource;
import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;
import solutions.misi.mysteryhunt.objects.Treasures;
import solutions.misi.mysteryhunt.plugin.DependencyManager;
import solutions.misi.mysteryhunt.plugin.Messages;
import solutions.misi.mysteryhunt.plugin.PluginHandler;
import solutions.misi.mysteryhunt.sql.MhPlayersTable;
import solutions.misi.mysteryhunt.sql.MhServerTable;
import solutions.misi.mysteryhunt.sql.MySQL;
import solutions.misi.mysteryhunt.util.FileManager;
import solutions.misi.mysteryhunt.util.LocationHandler;
import solutions.misi.mysteryhunt.util.SchedulerManager;

import java.util.HashMap;

public class MysteryHunt extends JavaPlugin {

    private static MysteryHunt instance;
    public SchedulerManager schedulerManager;
    public LocationHandler locationHandler;
    public FileManager fileManager;
    public Treasures treasures;
    public HashMap<Location, String> notDestroyable = new HashMap<>();
    public Messages messages;
    public DependencyManager dependencyManager;
    public MySQL mySQL;
    public HikariDataSource hikariDataSource;
    public MhPlayersTable playersTable;
    public MhServerTable serverTable;

    @Override
    public void onEnable() {
        instance = this;
        schedulerManager = new SchedulerManager();
        locationHandler = new LocationHandler();
        fileManager = new FileManager();
        treasures = new Treasures();
        messages = new Messages();
        dependencyManager = new DependencyManager();
        hikariDataSource = new HikariDataSource();
        mySQL = new MySQL();
        playersTable = new MhPlayersTable();
        serverTable = new MhServerTable();

        new PluginHandler();
        mySQL.initializeDatabase();
        dependencyManager.loadDependencies();
        schedulerManager.loadTreasures();
    }
	
	@Override
	public void onDisable() {
        treasures.destroyAllTreasures();
        mySQL.closeConnection();
    }
	
	public static MysteryHunt getInstance() {
		return instance;
	}
}
