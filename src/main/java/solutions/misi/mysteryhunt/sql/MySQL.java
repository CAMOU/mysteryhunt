package solutions.misi.mysteryhunt.sql;

import solutions.misi.mysteryhunt.MysteryHunt;

public class MySQL {

    private void loadLoginCredentials() {
        String host = MysteryHunt.getInstance().getConfig().getString("mysql.host");
        String port = MysteryHunt.getInstance().getConfig().getString("mysql.port");
        String database = MysteryHunt.getInstance().getConfig().getString("mysql.database");

        MysteryHunt.getInstance().hikariDataSource.setJdbcUrl("jdbc:mysql://" + host + ":" + port + "/" + database);
        MysteryHunt.getInstance().hikariDataSource.addDataSourceProperty("user", MysteryHunt.getInstance().getConfig().getString("mysql.username"));
        MysteryHunt.getInstance().hikariDataSource.addDataSourceProperty("password", MysteryHunt.getInstance().getConfig().getString("mysql.password"));
    }

    public void initializeDatabase() {
        loadLoginCredentials();
        MysteryHunt.getInstance().playersTable.create();
        MysteryHunt.getInstance().serverTable.create();
    }

    public void closeConnection() {
        if(MysteryHunt.getInstance().hikariDataSource != null) {
            MysteryHunt.getInstance().hikariDataSource.close();
        }
    }

    public void sendConnectionFailed(Exception ex) {
        /*if(ex != null)
            ex.printStackTrace();
        Bukkit.getConsoleSender().sendMessage(" §4(!) §fMysteryHunt couldn't start due to MySQL Connection issues."); */
    }
}