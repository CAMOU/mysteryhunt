package solutions.misi.mysteryhunt.sql;

import solutions.misi.mysteryhunt.MysteryHunt;

import java.sql.*;

public class MhServerTable {

    public void create() {
        try(Connection connection = MysteryHunt.getInstance().hikariDataSource.getConnection();
            Statement statement = connection.createStatement()) {
            statement.execute("CREATE TABLE IF NOT EXISTS mhserver (ID INT)");
        } catch(SQLException ex) {
            MysteryHunt.getInstance().mySQL.sendConnectionFailed(ex);
        }

        update();
    }

    private void update() {
        while (!checkColumns().equals("")) {
            String treasure = checkColumns();
            String sql = "ALTER TABLE mhserver ADD " + treasure + " INT AFTER ID";

            try(Connection connection = MysteryHunt.getInstance().hikariDataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.executeUpdate();
                System.out.println("MysteryHunt [] Added " + treasure + " Stats to MHSERVER table!");
            } catch(SQLException ex) {
                MysteryHunt.getInstance().mySQL.sendConnectionFailed(ex);
            }
        }

        String sql = "INSERT INTO mhserver (ID,";
        int treasureAmount = 0;

        for(String treasure : MysteryHunt.getInstance().getConfig().getConfigurationSection("treasures").getKeys(false)) {
            sql += treasure + ",";
            treasureAmount++;
        }

        sql = sql.substring(0, sql.length() - 1);
        sql += ") VALUES (?,";

        for(int i = 0; i < treasureAmount; i++) sql += "0,";

        sql = sql.substring(0, sql.length() - 1);
        sql += ")";

        try(Connection connection = MysteryHunt.getInstance().hikariDataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, 1);
            statement.executeUpdate();
        } catch(SQLException ex) {
            MysteryHunt.getInstance().mySQL.sendConnectionFailed(ex);
        }
    }

    private String checkColumns() {
        for(String treasure : MysteryHunt.getInstance().getConfig().getConfigurationSection("treasures").getKeys(false)) {
            try(Connection connection = MysteryHunt.getInstance().hikariDataSource.getConnection()) {
                DatabaseMetaData metaData = connection.getMetaData();
                ResultSet results = metaData.getColumns(null, null, "mhserver", treasure);
                if(!results.next()) {
                    //> Column does not exist
                    return treasure;
                }
            } catch(SQLException ex) {
                MysteryHunt.getInstance().mySQL.sendConnectionFailed(ex);
            }
        }

        return "";
    }

    public Integer getTreasuresOpened(String treasure) {
        try(Connection connection = MysteryHunt.getInstance().hikariDataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM mhserver")) {
            ResultSet results = statement.executeQuery();
            if(results.next()) {
                return results.getInt(treasure);
            }
        } catch(SQLException ex) {
            MysteryHunt.getInstance().mySQL.sendConnectionFailed(ex);
        }

        return -1;
    }

    public void setTreasuresOpened(String treasure, int amount) {
        try(Connection connection = MysteryHunt.getInstance().hikariDataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement("UPDATE mhserver SET " + treasure + " = ?")) {
            statement.setInt(1, amount);
            statement.executeUpdate();
        } catch(SQLException ex) {
            MysteryHunt.getInstance().mySQL.sendConnectionFailed(ex);
        }
    }
}