package solutions.misi.mysteryhunt.sql;

import org.bukkit.entity.Player;
import solutions.misi.mysteryhunt.MysteryHunt;

import java.sql.*;

public class MhPlayersTable {

    public void create() {
        try(Connection connection = MysteryHunt.getInstance().hikariDataSource.getConnection();
            Statement statement = connection.createStatement()) {
            statement.execute("CREATE TABLE IF NOT EXISTS mhplayers (UUID VARCHAR(255) PRIMARY KEY)");
        } catch(SQLException ex) {
            MysteryHunt.getInstance().mySQL.sendConnectionFailed(ex);
        }

        update();
    }

    private void update() {
        while (!checkColumns().equals("")) {
            String treasure = checkColumns();
            String sql = "ALTER TABLE mhplayers ADD " + treasure + " INT AFTER uuid";

            try(Connection connection = MysteryHunt.getInstance().hikariDataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.executeUpdate();
                System.out.println("MysteryHunt [] Added " + treasure + " Stats to MHPLAYERS table!");
            } catch(SQLException ex) {
                MysteryHunt.getInstance().mySQL.sendConnectionFailed(ex);
            }
        }
    }

    public void registerPlayer(Player player) {
        String sql = "INSERT INTO mhplayers (UUID,";
        int treasureAmount = 0;

        for(String treasure : MysteryHunt.getInstance().getConfig().getConfigurationSection("treasures").getKeys(false)) {
            sql += treasure + ",";
            treasureAmount++;
        }

        sql = sql.substring(0, sql.length() - 1);
        sql += ") VALUES (?,";

        for(int i = 0; i < treasureAmount; i++) sql += "0,";

        sql = sql.substring(0, sql.length() - 1);
        sql += ")";

        try(Connection connection = MysteryHunt.getInstance().hikariDataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, player.getUniqueId().toString());
            statement.executeUpdate();
        } catch(SQLException ex) {
            MysteryHunt.getInstance().mySQL.sendConnectionFailed(ex);
        }
    }

    private String checkColumns() {
        for(String treasure : MysteryHunt.getInstance().getConfig().getConfigurationSection("treasures").getKeys(false)) {
            try(Connection connection = MysteryHunt.getInstance().hikariDataSource.getConnection()) {
                DatabaseMetaData metaData = connection.getMetaData();
                ResultSet results = metaData.getColumns(null, null, "mhplayers", treasure);
                if(!results.next()) {
                    //> Column does not exist
                    return treasure;
                }
            } catch(SQLException ex) {
                MysteryHunt.getInstance().mySQL.sendConnectionFailed(ex);
            }
        }

        return "";
    }

    public Integer getTreasuresOpened(Player player, String treasure) {
        try(Connection connection = MysteryHunt.getInstance().hikariDataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM mhplayers WHERE UUID = ?")) {
            statement.setString(1, player.getUniqueId().toString());
            ResultSet results = statement.executeQuery();
            if(results.next()) {
                return results.getInt(treasure);
            }
        } catch(SQLException ex) {
            MysteryHunt.getInstance().mySQL.sendConnectionFailed(ex);
        }

        return -1;
    }

    public void setTreasuresOpened(Player player, String treasure, int amount) {
        try(Connection connection = MysteryHunt.getInstance().hikariDataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement("UPDATE mhplayers SET " + treasure + " = ? WHERE UUID = ?")) {
            statement.setInt(1, amount);
            statement.setString(2, player.getUniqueId().toString());
            statement.executeUpdate();
        } catch(SQLException ex) {
            MysteryHunt.getInstance().mySQL.sendConnectionFailed(ex);
        }
    }
}