package solutions.misi.mysteryhunt.plugin;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import solutions.misi.mysteryhunt.MysteryHunt;

public class Messages {

	public String prefix = ChatColor.translateAlternateColorCodes('&', MysteryHunt.getInstance().getConfig().getString("messages.prefix"));

	public void sendHelpMessage(Player player) {

		for(String entry : MysteryHunt.getInstance().getConfig().getStringList("messages.help")) {
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', entry));
		}

	}
}
