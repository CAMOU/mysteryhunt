package solutions.misi.mysteryhunt.plugin;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import solutions.misi.mysteryhunt.MysteryHunt;
import solutions.misi.mysteryhunt.commands.HuntingToolCommand;
import solutions.misi.mysteryhunt.commands.MysteryHuntCommand;
import solutions.misi.mysteryhunt.commands.SpawnTreasureCommand;
import solutions.misi.mysteryhunt.events.BlockBreakListener;
import solutions.misi.mysteryhunt.events.PlayerArmorStandManipulateListener;
import solutions.misi.mysteryhunt.events.PlayerInteractListener;
import solutions.misi.mysteryhunt.events.PlayerJoinListener;
import solutions.misi.mysteryhunt.expansion.PointsExpansion;

import java.io.File;
import java.io.IOException;

public class PluginHandler {

	private void loadCommands() {
        HuntingToolCommand cHuntingToolCMD = new HuntingToolCommand();
        MysteryHunt.getInstance().getCommand("huntingtool").setExecutor(cHuntingToolCMD);
        SpawnTreasureCommand cSpawnTreasureCommand = new SpawnTreasureCommand();
        MysteryHunt.getInstance().getCommand("spawntreasure").setExecutor(cSpawnTreasureCommand);
        MysteryHuntCommand cMysteryHuntCommand = new MysteryHuntCommand();
        MysteryHunt.getInstance().getCommand("mysteryhunt").setExecutor(cMysteryHuntCommand);
    }
	
	private void registerEvents() {
        Bukkit.getPluginManager().registerEvents(new PlayerInteractListener(), MysteryHunt.getInstance());
        Bukkit.getPluginManager().registerEvents(new BlockBreakListener(), MysteryHunt.getInstance());
        Bukkit.getPluginManager().registerEvents(new PlayerArmorStandManipulateListener(), MysteryHunt.getInstance());
        Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), MysteryHunt.getInstance());
    }
	
	private void createFiles() {
		MysteryHunt.getInstance().saveDefaultConfig();
		
		File worlds = new File("plugins/MysteryHunt/worlds.yml");
		FileConfiguration worldsCfg = YamlConfiguration.loadConfiguration(worlds);
		
		if(!worlds.exists()) {
			try {
				worlds.createNewFile();
				worldsCfg.set("world.spawn.X", 0);
				worldsCfg.set("world.spawn.Y", 0);
				worldsCfg.set("world.spawn.Z", 0);
				worldsCfg.set("world.max-chests-overall", 50);
				worldsCfg.set("world.max-chests.basic", 10);
				worldsCfg.save(worlds);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void placeholderAPI() {
		if (MysteryHunt.getInstance().dependencyManager.getPlaceholderApi()) {
			PointsExpansion pointsExpansion = new PointsExpansion(MysteryHunt.getInstance());
			pointsExpansion.register();
		}
	}
	
	public PluginHandler() {
		createFiles();
		loadCommands();
		registerEvents();
		placeholderAPI();
	}
}
