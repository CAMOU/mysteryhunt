package solutions.misi.mysteryhunt.commands;

import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import solutions.misi.mysteryhunt.MysteryHunt;
import solutions.misi.mysteryhunt.objects.Parachute;

import java.util.List;

public class SpawnTreasureCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 1) {
            String treasure = args[0];
            final int minPlayers = MysteryHunt.getInstance().getConfig().getInt("treasures." + treasure + ".min-players");
            final List<String> worldsList = (List<String>) MysteryHunt.getInstance().getConfig().get("treasures." + treasure + ".spawn-worlds");
            final boolean fallFromSky = MysteryHunt.getInstance().getConfig().getBoolean("treasures." + treasure + ".fall-from-sky");

            if (MysteryHunt.getInstance().getConfig().getString("treasures." + treasure + ".broadcast") != null) {
                if (sender instanceof Player) {
                    Player p = (Player) sender;

                    if (!p.hasPermission("mysteryhunt.spawntreasure") && !p.isOp()) {
                        p.sendMessage(MysteryHunt.getInstance().messages.prefix + " §cYou don't have enough permissions!");
                        return false;
                    }
                }

                for (int i = 0; i < worldsList.size(); i++) {
                    int playersInWorld = 0;
                    for (Player all : Bukkit.getOnlinePlayers()) {
                        if (all.getWorld().getName().equals(worldsList.get(i))) {
                            playersInWorld = playersInWorld + 1;
                        }
                    }

                    if (playersInWorld >= minPlayers) {
                        String broadcast = ChatColor.translateAlternateColorCodes('&', MysteryHunt.getInstance().getConfig().getString("treasures." + treasure + ".broadcast")).replaceAll("%world%", worldsList.get(i));
                        int maxChests = MysteryHunt.getInstance().fileManager.getFileConfiguratioon(MysteryHunt.getInstance().fileManager.getWorldsFile()).getInt(worldsList.get(i) + ".max-chests");

                        if (MysteryHunt.getInstance().dependencyManager.getPlaceholderApi()) {
                            broadcast = PlaceholderAPI.setPlaceholders(null, broadcast);
                        }

                        int currentChests = 0;
                        for (Location loc : MysteryHunt.getInstance().treasures.treasures.keySet()) {
                            if (loc.getWorld().getName().equals(worldsList.get(i))) {
                                currentChests++;
                            }
                        }

                        if (currentChests <= maxChests) {
                            Location loc = MysteryHunt.getInstance().locationHandler.findTreasureLocation(worldsList.get(i), treasure).add(0, 2, 0);

                            if (fallFromSky) {
                                new Parachute(worldsList.get(i), loc.getX(), (loc.getY() + 50), loc.getZ(), treasure);
                            } else {
                                Material blockUnderneath = Material.getMaterial(MysteryHunt.getInstance().getConfig().getString("treasures." + treasure + ".block-underneath"));

                                Bukkit.getWorld(worldsList.get(i)).getBlockAt(loc).setType(Material.CHEST);
                                Bukkit.getWorld(worldsList.get(i)).getBlockAt(loc.clone().subtract(0, 1, 0)).setType(blockUnderneath);

                                MysteryHunt.getInstance().treasures.treasures.put(new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()), treasure);
                            }

                            MysteryHunt.getInstance().notDestroyable.put(loc, treasure);
                            MysteryHunt.getInstance().schedulerManager.despawnNaturalTreasure(loc, treasure);
                            Bukkit.broadcastMessage(MysteryHunt.getInstance().messages.prefix + broadcast);
                        }
                    }
                }
            } else {
                sender.sendMessage(MysteryHunt.getInstance().messages.prefix + "§cThe treasure " + args[0] + " doesn't exist!");
            }
        } else {
            sender.sendMessage(MysteryHunt.getInstance().messages.prefix + "§cPlease use /spawntreasure <treasure>!");
        }

        return false;
    }
}
