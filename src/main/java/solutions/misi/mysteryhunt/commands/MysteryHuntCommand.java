package solutions.misi.mysteryhunt.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import solutions.misi.mysteryhunt.MysteryHunt;

public class MysteryHuntCommand implements CommandExecutor {

    //> /mysteryhunt
    //> /mysteryhunt removeall
    //> /mysteryhunt remove <treasure>
    //> /mysteryhunt set <player> <treasure> <amount>
    //> /mysteryhunt count
    //> /mysteryhunt treasures

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command cmd, @NotNull String label, @NotNull String[] args) {
        if(!(sender instanceof Player)) return false;

        Player player = (Player) sender;

        if(args.length == 0 || args.length > 4) {
            MysteryHunt.getInstance().messages.sendHelpMessage(player);
            return true;
        }

        switch(args[0].toLowerCase()) {
            case "removeall":
                if(!player.hasPermission("mysteryhunt.remove") && !player.isOp()) {
                    player.sendMessage(MysteryHunt.getInstance().messages.prefix + " §cYou don't have enough permissions!");
                    return false;
                }

                MysteryHunt.getInstance().treasures.destroyAllTreasures();
                player.sendMessage(MysteryHunt.getInstance().messages.prefix + " §aAll treasures have been destroyed!");
                break;
            case "remove":
                if(!player.hasPermission("mysteryhunt.remove") && !player.isOp()) {
                    player.sendMessage(MysteryHunt.getInstance().messages.prefix + " §cYou don't have enough permissions!");
                    return false;
                }

                String treasure = args[1];

                MysteryHunt.getInstance().treasures.destroyAllTreasures(treasure);
                player.sendMessage(MysteryHunt.getInstance().messages.prefix + " §aAll " + treasure + " treasures have been destroyed!");
                break;
            case "count":
                if(!player.hasPermission("mysteryhunt.count") && !player.isOp()) {
                    player.sendMessage(MysteryHunt.getInstance().messages.prefix + " §cYou don't have enough permissions!");
                    return false;
                }

                String msg = MysteryHunt.getInstance().getConfig().getString("messages.commands.mysteryhunt-count");
                msg = msg.replace("%count%", Integer.valueOf(MysteryHunt.getInstance().treasures.treasures.size()).toString());
                msg = ChatColor.translateAlternateColorCodes('&', msg);
                player.sendMessage(msg);
                break;
            case "treasures":
                if(!player.hasPermission("mysteryhunt.treasures") && !player.isOp()) {
                    player.sendMessage(MysteryHunt.getInstance().messages.prefix + " §cYou don't have enough permissions!");
                    return false;
                }

                player.sendMessage(ChatColor.translateAlternateColorCodes('&', MysteryHunt.getInstance().getConfig().getString("messages.commands.mysteryhunt-treasures.active")));

                for (org.bukkit.Location location : MysteryHunt.getInstance().treasures.treasures.keySet()) {
                    Location key = location;
                    String msg2 = MysteryHunt.getInstance().getConfig().getString("messages.commands.mysteryhunt-treasures.keys");
                    msg2 = msg2.replace("%x%", Integer.valueOf(key.getBlockX()).toString());
                    msg2 = msg2.replace("%y%", Integer.valueOf(key.getBlockY()).toString());
                    msg2 = msg2.replace("%z%", Integer.valueOf(key.getBlockZ()).toString());
                    msg2 = msg2.replace("%world%", key.getWorld().getName());
                    msg2 = msg2.replace("%type%", MysteryHunt.getInstance().treasures.treasures.get(key));
                    msg2 = ChatColor.translateAlternateColorCodes('&', msg2);
                    player.sendMessage(msg2);
                }

                break;
            case "set":
                if(!player.hasPermission("mysteryhunt.set") && !player.isOp()) {
                    player.sendMessage(MysteryHunt.getInstance().messages.prefix + " §cYou don't have enough permissions!");
                    return false;
                }

                if(args.length != 4) {
                    player.sendMessage(MysteryHunt.getInstance().messages.prefix + "§cUse /mh set <player> <treasure> <amount>");
                    return false;
                }

                Player target = Bukkit.getPlayer(args[1]);

                if(target == null) {
                    player.sendMessage(MysteryHunt.getInstance().messages.prefix + "§cThe player " + args[1] + " is not online");
                    return false;
                }

                String treasure1 = args[2];

                if(MysteryHunt.getInstance().getConfig().getString("treasures." + treasure1 + ".countdown") == null) {
                    player.sendMessage(MysteryHunt.getInstance().messages.prefix + "§cThe treasure " + treasure1 + " is not online");
                    return false;
                }

                try {
                    int amount = Integer.parseInt(args[3]);
                    int previousAmount = MysteryHunt.getInstance().playersTable.getTreasuresOpened(player, treasure1);

                    MysteryHunt.getInstance().playersTable.setTreasuresOpened(player, treasure1, amount);

                    if(previousAmount > amount) {
                        // subtract
                        MysteryHunt.getInstance().serverTable.setTreasuresOpened(treasure1, MysteryHunt.getInstance().serverTable.getTreasuresOpened(treasure1) - (previousAmount-amount));
                    } else {
                        // add
                        MysteryHunt.getInstance().serverTable.setTreasuresOpened(treasure1, MysteryHunt.getInstance().serverTable.getTreasuresOpened(treasure1) + (previousAmount-amount));
                    }
                } catch(NumberFormatException ex) {
                    player.sendMessage(MysteryHunt.getInstance().messages.prefix + "§cPlease specify a real number!");
                    return false;
                }

                break;
            default:
                MysteryHunt.getInstance().messages.sendHelpMessage(player);
                return false;
        }

        return true;
    }
}
