package solutions.misi.mysteryhunt.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import solutions.misi.mysteryhunt.MysteryHunt;
import solutions.misi.mysteryhunt.objects.HuntingTool;

public class HuntingToolCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player) sender;

			if(!p.hasPermission("mysteryhunt.huntingtool") && !p.isOp()) {
				p.sendMessage(MysteryHunt.getInstance().messages.prefix + " §cYou don't have enough permissions!");
				return false;
			}

			if (args.length == 1 && MysteryHunt.getInstance().getConfig().get("huntingtools." + args[0] + ".name") != null) {
				p.getInventory().addItem(new ItemStack(HuntingTool.getHuntingTool(args[0])));
				return true;
			}

			MysteryHunt.getInstance().messages.sendHelpMessage(p);
		}

		return false;
	}
}
